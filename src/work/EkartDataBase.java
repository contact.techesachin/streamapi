package work;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EkartDataBase {

    public static List<Customer> getAll(){
        return Stream.of(
                new Customer(101, "sipu", "sipu@gmail.com", Arrays.asList("7004634177", "7100e8364")),
                new Customer(102, "sudhir", "sudhir@gmail.com", Arrays.asList("6300366542", "82353492")),
                new Customer(103, "buchani", "buchani@gmail.com", Arrays.asList("8004634177", "92873541")),
                new Customer(104, "siwani", "siwani@gmail.com", Arrays.asList("9004634177", "982634651"))
        ).collect(Collectors.toList());
    }
}
