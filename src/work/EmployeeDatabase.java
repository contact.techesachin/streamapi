package work;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class EmployeeDatabase {
    public static List<Employee> getEmployees(){
        return Stream.of(
        new Employee(101, "john", "A", 60000.0),
        new Employee(102, "peter", "B", 80000),
        new Employee(103, "mak", "C", 50000),
        new Employee(104, "kim", "A", 70000),
        new Employee(105, "json", "D", 90000))
        .collect(Collectors.toList());
    }
}
