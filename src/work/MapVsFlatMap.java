package work;

import java.util.List;
import java.util.stream.Collectors;

public class MapVsFlatMap {
    public static void main(String[] args) {
        List<Customer> customers = EkartDataBase.getAll();

        //List<Customer>  covert List<String> -> Data transformation
        //mapping  : customer -> getEmail()
        // customer -> customer.getEmail() one to one mapping.
        List<String> email = customers.stream().map(customer -> customer.getEmail()).collect(Collectors.toList());
        System.out.println(email);

        //customer.getPhoneNumbers() ->> one to many mapping
        List<List<String>> mobileNumber = customers.stream().map(customer -> customer.getPhoneNumbers()).collect(Collectors.toList());
        System.out.println(mobileNumber);

        //List<Customer>  covert List<String> -> Data transformation
        //mapping  : customer -> PhoneNumbers()
        //customer.getPhoneNumbers() ->> one to many mapping
        List<String> phones = customers.stream()
                .flatMap(customer -> customer
                .getPhoneNumbers().stream())
                .collect(Collectors.toList());
        System.out.println(phones);
    }
}
